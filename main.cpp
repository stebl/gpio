#include "gpio.h"
#include <fstream>
#include <iostream>
#include <sstream>
using namespace std;

// Define output pins (constant)
const int green = 314;
    
int main(int argc, char** argv) {
    GPIO myGPIO;
    
    // Loop exit condition
    bool exit = false;   
    
    // Initialize green pin
    myGPIO.initGPIO(green);
    
    // While we want to loop...
    while(!exit){
      // Toggle pin on
      myGPIO.setGPIO(green, 1);
      
      // Wait
      for(int i = 0; i < 100000000; i++){};
      
      // Toggle pin off
      myGPIO.setGPIO(green, 0);
      
      // Wait
      for(int i = 0; i < 100000000; i++){};
      
      // Update exit (if desired)
      
    }
    return 0;
}
