#include "gpio.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

void GPIO::initGPIO(int pin){
  // Definitions
  string myString;
     
  // Setup file stream for export
  ofstream exportFile;
  exportFile.open ( "/sys/class/gpio/export" ); 
  
  // Write pin to export file
  exportFile << pin;
  
  // Close stream
  exportFile.close();
  
  // Setup port as output
  ofstream portFile;
  
  std::stringstream ss;
  ss << "/sys/class/gpio/gpio" << pin << "/direction";
  ss >> myString;
  portFile.open(myString.c_str());
  
  // Initialize as low output
  portFile << "low";
  
  // Close the stream
  portFile.close();
}

void GPIO::setGPIO(int pin, int state)
{
  // Definitions
  string myString;
  
  // Define pin stream
  ofstream pinStream;

  std::stringstream ss;
  ss << "/sys/class/gpio/gpio" << pin << "/value";
  ss >> myString;
  pinStream.open (myString.c_str());
  
  // Write to pin stream
  pinStream << state;
  
  // Close pin stream
  pinStream.close();
}

GPIO::~GPIO()
{
	
}
